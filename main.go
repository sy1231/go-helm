package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

func world() string {
	return "world"
}

func resetHandler(w http.ResponseWriter, r *http.Request) {
	host, err := os.Hostname()
	if err != nil {
		panic(err)
	}
	dt := time.Now()
	fmt.Fprintf(w, `
	hello %s
	Hostname: %s
	Now: %s
	`, world(), host, dt)
}

func main() {
	p := "8000"
	args := os.Args[1:]
	if len(args) < 1 {
		// panic("You should provide port")
	} else {
		p = args[0]
	}
	// url := fmt.Sprintf("localhost:%s", p)
	url := fmt.Sprintf(":%s", p)

	http.HandleFunc("/", resetHandler)
	log.Fatal(http.ListenAndServe(url, nil))
}
